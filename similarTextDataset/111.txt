Bachelor projects
Below you find a list of possible bachelor projects. Students in search of more general information concerning the Bachelor projects: please consult the Blackboard site or contact Erik van Heumen.
Institute for Theoretical Physics Amsterdam [IoP-ITFA]
Projects at IoP-ITFA 2016 (.pdf)
Institute for High Energy Physics [IoP-IHEF] / Nikhef
Projects at IoP-IHEF (wiki)
Van der Waals-Zeeman Institute [IoP-WZI]
Projects at IoP-WZI 2016 (.pdf)
In adition to these projects there is a collaborative project available involving the group of Prof. Dr. D. Bonn (WZI) and the group of Prof. Dr. S. Woutersen (HIMS).
WZI-HIMS project 2016 (.pdf)
