Re-enrolment
How to re-enrol in a Bachelor’s or Master’s programme
At the end of the academic year, you will need to re-enrol for the next year of your Bachelor’s or Master’s programme.
Re-enrolment steps
The re-enrolment process includes four steps. You will start by re-enrolling via Studielink. And you can complete your re-enrolment by arranging the payment of your tuition fees.
Find out more about each step by following the instructions for re-enrolling in a Bachelor’s or Master’s programme below.
Re-enrol in a Bachelor's programme
Re-enrol in a Master's programme
Students at table
Photographer: Sander Nieuwenhuys
Changing your enrolment
You can also make changes to your enrolment via Studielink.
Find out how to change your enrolment
