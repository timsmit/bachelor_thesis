Chemistry: Analytical Sciences (track)
In the two-year track Analytical Sciences of the Master’s programme Chemistry, a joint degree programme with Vrije Universiteit Amsterdam, you will focus purely on aspects of analytical chemistry, such as modern analytical techniques, general concepts and strategies for analysis and various application fields.

Request brochure

Analytical chemistry plays a crucial role in many areas of science and society. Chemical compounds in a variety of materials and environments must be identified and quantified for, for example:
the development of new pharmaceuticals
the unravelling of complex biological systems
the design of new sustainable materials and chemical processes
Analytical chemistry provides the methods and techniques that make this possible. Its impact is not restricted to chemistry alone: progress in medicine, biology, environmental and material sciences is strongly dependent on the availability of new analytical tools as well.
Is Analytical Sciences in Amsterdam the track for me?
It is, if you:
have a solid background in chemistry
are eager to expand your knowledge and skills in the area of analytical chemistry
want the advantages of a small-scale programme, where students and staff all know each other.
What does Analytical Sciences in Amsterdam have to offer me?
This Master's track is unique in the Netherlands, in that it focuses purely on (all aspects of) analytical chemistry - a specialisation for which there is currently a great demand on the labour market. It covers:
a large range of modern analytical techniques, such as chromatography, mass spectrometry, spectroscopy and laser-based approaches
general concepts and strategies for analysis, from sampling to data treatment
various application fields, such as biochemical, industrial, pharmaceutical and forensic analysis
The track also offers the possibility to focus specifically on environmental chemistry and analysis.

Joint degree programme with VU Amsterdam
Analytical Sciences is a track in the Master's programme Chemistry, a joint degree programme of the University of Amsterdam and Vrije Universiteit Amsterdam. Courses are taught at locations of both universities. UvA and VU jointly issue a degree certificate to graduates. As a Chemistry student in Amsterdam you benefit from expertise, networks and research projects at both universities and affiliated research institutes.
