Tuition fees and costs
MSc in Political Theory (Political Science)
As a student in the Netherlands you will pay an annual tuition fee. There are two different fees: the statutory fee and the institutional fee. Which fee you pay depends on your nationality, your degree programme and whether you study full-time, part-time or dually.
Tuition fees*
Full-time tuition fees for European Economic Area students (first master's): € 2.006,-
Full-time tuition fees for non-European Economic Area students  € 14.740,-
*These are the tuition fees for academic year 2017-2018.
You can calculate your tuition fee for this programme by using the Tuition fee calculator. Please note: the calculator does not work in Safari.
Tuition fee calculator Political Theory
Study costs
In addition to your tuition fees, you will need to include a number of other study costs in your budget, such as books, stationary, excursions, a laptop or other equipment. These costs vary per programme but academic books and study materials will usually cost you between €50-100 per month.
Living costs
Keep in mind that living in Amsterdam also includes costs such as accommodation, food and public transport.
Read more about living expenses in Amsterdam
You can find all the terms and conditions regarding enrolment at the UvA in the Enrolment provisions.
