Chemistry (joint degree UvA/VU)
The Master's programme Chemistry, a joint degree with Vrije Universiteit Amsterdam, provides you with a highly qualified background of theory and practice in chemistry and sufficient specialisation in a specific field. It exposes you to research at the front line of this exciting, multifaceted discipline that forms the bridge between biology at one end of the spectrum, and physics and mathematics at the other. Globally, chemistry is acknowledged as playing a key role in social and economic progress.


msc-chemistry-general-landing
Joint degree programme with VU Amsterdam
Chemistry is a joint degree programme of the University of Amsterdam and Vrije Universiteit Amsterdam. Courses are taught at locations of both universities. UvA and VU jointly issue a degree certificate to graduates. As a Chemistry student in Amsterdam you benefit from expertise, networks and research projects at both universities and affiliated research institutes.
UvA-VU logo
Tracks
Within the Master's programme Chemistry, every student can find an area suited to his or her individual talents and ambitions. There are several different tracks:
Analytical Sciences
Molecular Sciences*
Science for Energy and Sustainability
Starting from September 2016, the former Chemistry Master’s tracks ‘Molecular Design, Synthesis and Catalysis' and 'Molecular Simulation and Photonics’ have been recombined into the track ‘Molecular Sciences’.
