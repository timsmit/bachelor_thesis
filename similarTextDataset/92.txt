Examination arrangements - PSY
Special examination arrangements for Psychology
The regulations for Psychology degree programme students with a disability, and the special examination arrangements, are as follows.
When are you eligible for a special arrangement?
Being eligible for a special examination arrangement depends on your disability. The arrangement must be necessary to sit the examination, but should not conflict with the learning outcomes for the degree programme. Contact one of the student counsellors to discuss your options.
When do the special arrangements granted apply?
The special arrangements at Psychology only apply to:
examinations that you sit during the period for which the arrangement has been granted;
examinations for courses attended in Psychology. If you also take courses in another programme, you should ask whether you can make use of the special arrangement for these, and, if so, how;
official examinations (on location) and not for (interim) tests in the classroom or writing assignments.
Do I need to let the study programme know that I have been granted extra time to complete an examination?
For the compulsory first and second year Bachelor's courses, the extra examination time is granted automatically where applicable. The examinations coordinator obtains the list of students who are eligible for extra examination time from SIS.
If you are taking an elective in Psychology or you are in your specialisation year or taking a Master's programme, you should let the Secretariat of your department know that you have the right to extra time to complete your examination. This will then be taken into account during the examinations.
What factors do you need to take into account?
If you apply for additional examination time, you will be given an additional 30 minutes for each examination.
You will sit the examination at the same location as regular students, unless otherwise agreed with your study adviser (customised ‘Maatwerk’ arrangement).
Always report to the invigilator at the start of the examination, so he/she can tell you where to sit.
If you have been granted the customised (‘Maatwerk’) arrangement, you should discuss with the study adviser (for the degree programme in which you are taking courses) how the additional arrangement can be implemented.
If you do not intend to sit an examination for a course for which you are registered, you must report to the Student Desk to cancel the special arrangement at least two weeks before the examination concerned.
Questions?
If you have any questions about this policy or the rules, please do not hesitate to contact the Study Adviser: studieadviseur-psy-fmg@uva.nl.
