from os import listdir
from pickle import dump
import time
start_time = time.time()

# load doc into memory
def load_doc(filename):
	# open the file as read only
	file = open(filename, encoding='utf-8')
	text = file.read()
	file.close()
	return text

# split a document into news story and highlights
def split_story(doc):
	# find first highlight
	index = doc.find('@highlight')
	# split into story and highlights
	story, highlights = doc[:index], doc[index:].split('@highlight')
	# strip extra white space around each highlight
	highlights = [h.strip() for h in highlights if len(h) > 0]
	return story, highlights

def appendDict(dict, sentence):
	list = sentence.split(" ")
	for item in list:
		item = item.lower()
		if item not in ["the","or","and","of","to","a","in","is","it","you","that","he","was","for","on","are","with","as","i","his","they","be","at"]:
			if item in dict:
				dict[item] += (1)
			else:
				dict[item] = (1)
	return dict

# load all stories in a directory
def load_stories(directory):
	stories = list()
	stories1 = list()
	stories2 = list()
	stories3 = list()
	stories4 = list()
	stories5 = list()
	stories6 = list()
	stories7 = list()
	stories8 = list()
	totaldict = {}
	for name in listdir(directory):
		highlightDict = {}
		filename = directory + '/' + name
		# load document
		doc = load_doc(filename)
		# split into story and highlights
		story, highlights = split_story(doc)
		for high in highlights:
			highlightDict = appendDict(highlightDict, high)
			totaldict = appendDict(totaldict, high)
		if "president" in highlightDict:
			stories1.append({'story':story, 'highlights':highlightDict})
		if "school" in highlightDict:
			stories2.append({'story':story, 'highlights':highlightDict})
		if "world" in highlightDict:
			stories3.append({'story':story, 'highlights':highlightDict})
		if "killed" in highlightDict:
			stories4.append({'story':story, 'highlights':highlightDict})
		if "court" in highlightDict:
			stories5.append({'story':story, 'highlights':highlightDict})
		if "u.s." in highlightDict:
			stories6.append({'story':story, 'highlights':highlightDict})
		if "security" in highlightDict:
			stories7.append({'story':story, 'highlights':highlightDict})
		if "students" in highlightDict:
			stories8.append({'story':story, 'highlights':highlightDict})
		stories.append({'story':story, 'highlights':highlightDict})
		# for highlight in highlights:
		#  	highlightDict = appendDict(highlightDict, highlight)

	for i, item in enumerate(reversed(sorted(totaldict, key=totaldict.__getitem__))):
		print(item)
		if i > 200:
			break
	return (stories1, stories2, stories3, stories4, stories5, stories6, stories7, stories8)

# clean a list of lines
def clean_lines(lines):
	cleaned = list()
	# prepare a translation table to remove punctuation
	table = str.maketrans('', '', string.punctuation)
	for line in lines:
		# strip source cnn office if it exists
		index = line.find('(CNN) -- ')
		if index > -1:
			line = line[index+len('(CNN)'):]
		# tokenize on white space
		line = line.split()
		# convert to lower case
		line = [word.lower() for word in line]
		# remove punctuation from each token
		line = [w.translate(table) for w in line]
		# remove tokens with numbers in them
		line = [word for word in line if word.isalpha()]
		# store as string
		cleaned.append(' '.join(line))
	# remove empty strings
	cleaned = [c for c in cleaned if len(c) > 0]
	return cleaned

# load stories
directory = 'cnn/stories/'
(stories1, stories2, stories3, stories4, stories5, stories6, stories7, stories8) = load_stories(directory)
print('Loaded Stories 1 %d' % len(stories1))
print('Loaded Stories 2 %d' % len(stories2))
print('Loaded Stories 3 %d' % len(stories3))
print('Loaded Stories 4 %d' % len(stories4))
print('Loaded Stories 5 %d' % len(stories5))
print('Loaded Stories 6 %d' % len(stories6))
print('Loaded Stories 7 %d' % len(stories7))
print('Loaded Stories 8 %d' % len(stories8))
dump(stories1, open('cnn_dataset1.pkl', 'wb'))
dump(stories2, open('cnn_dataset2.pkl', 'wb'))
dump(stories3, open('cnn_dataset3.pkl', 'wb'))
dump(stories4, open('cnn_dataset4.pkl', 'wb'))
dump(stories5, open('cnn_dataset5.pkl', 'wb'))
dump(stories6, open('cnn_dataset6.pkl', 'wb'))
dump(stories7, open('cnn_dataset7.pkl', 'wb'))
dump(stories8, open('cnn_dataset8.pkl', 'wb'))
dump(stories, open('cnn_dataset.pkl', 'wb'))

print ("My program took", time.time() - start_time, "to run")

# load first 5 stories
# directory = 'cnn_small/stories/'
# stories = load_stories(directory)
# print('Loaded Stories %d' % len(stories))
# print(stories[4]['story'])
# print(stories[4]['highlights'])
# dump(stories, open('cnn_dataset_small.pkl', 'wb'))

# clean stories
# for example in stories:
# 	example['story'] = clean_lines(example['story'].split('\n'))
# 	example['highlights'] = clean_lines(example['highlights'])

# save to file
