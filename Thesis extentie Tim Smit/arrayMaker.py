import pickle
import math
import time
import numpy as np
import matplotlib.pyplot as plt



from subtraction import preprocesNGramSubtractionTest

def overlap(dict1, dict2):
	list1 = []
	for item in dict1:
		if item not in dict2:
			list1 += [item]
	return(list1)

def makeArray(a):
	array = []
	for (item, word) in a:
		array += [word]
	return array

# def comparer(a,b):
# 	arrayA, arrayB = [],[]
# 	for item in a[-10:]:
# 		if item not in b[-10:]:
# 			arrayA += [item]
# 	for item in b[-10:]:
# 		if item not in a[-10:]:
# 			arrayB += [item]
# 	return (a[-5:],b[-5:])

# dataDict = pickle.load( open( "permutations.pkl", "rb" ) )
# listPlot = []
# for p1 in range(2,6):
# 	listp1 = []
# 	for p2 in range(1,10):
# 		een, twee, drie, vier = 0,0,0,0
# 		for dataPoint in dataDict:
# 			(a,b) = preprocesNGramSubtractionTest(dataPoint[2],dataPoint[3],p1,p2/10)
# 			(a,b) = makeArray(a)[-5:],makeArray(b)[-5:]
# 			high1,high2 = overlap(dataPoint[0],dataPoint[1]), overlap(dataPoint[1],dataPoint[0])
# 			# high1,high2 = dataPoint[0],dataPoint[1]
# 			acount = 0
# 			for item in a:
# 				if item in high1:
# 					acount +=1
# 			bcount = 0
# 			for item in b:
# 				if item in high2:
# 					bcount +=1
# 			twee += acount+bcount
# 			een +=1
# 			if een is 100:
# 				break
# 		listp1 += [twee]
# 	print(listp1)
# 	listPlot += [listp1]
#
# print(listPlot)

H = np.array([[2.13, 2.13, 2.10, 2.13, 2.10, 2.10, 2.10, 2.13, 2.13], [2.06, 2.06, 2.02, 2.06, 2.02, 2.02, 2.02, 2.06, 2.06], [2.05, 2.05, 2.00, 2.05, 2.00, 2.00, 2.02, 2.05, 2.03], [1.93, 1.93, 1.86, 1.93, 1.86, 1.86, 1.88, 1.93, 1.89]])

fig = plt.figure(figsize=(9, 6))
ax = [1,2,3,4,5,6]
ax.set_title('Boundary check')
plt.grid(True)
plt.ylim((2,6))
plt.xlim((0,10))
plt.imshow(H)

cax = fig.add_axes([0.12, 0.1, 0.78, 0.8])
cax.get_xaxis().set_visible(False)
cax.get_yaxis().set_visible(False)
cax.patch.set_alpha(0)
cax.set_frame_on(False)
plt.colorbar(orientation='vertical')
ax.set_aspect('equal')
plt.show()
