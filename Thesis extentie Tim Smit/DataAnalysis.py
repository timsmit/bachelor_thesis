import pickle
import math
import time
import numpy as np
from pickle import dump
start_time = time.time()

permutations = list()
for y in range(1,9):
    dataList = pickle.load( open( "cnn_dataset" + str(y)+ ".pkl", "rb" ) )
    for i in range(0,len(dataList)):
        len1 = len(dataList[i]["highlights"])
        for j in range(1,len(dataList) - i):
            count = 0
            len2 = len(dataList[j+i]["highlights"])
            for key in dataList[i]["highlights"]:
                if key in dataList[j+i]["highlights"]:
                    count += 1
            if (count/((len1 + len2)/2.0) > 0.3 and count is not len1 and count is not len2):
                permutations.append([dataList[i]["highlights"],dataList[i+j]["highlights"],dataList[i]["story"],dataList[i+j]["story"]])
    print ("My program took", time.time() - start_time, "to run")
print('Loaded Stories 8 %d' % len(permutations))
dump(permutations, open('permutations.pkl', 'wb'))





# def makeSimArray(text1,text2):
#     text1 = text1.split()
#     text2 = text2.split()
#     print(text1)
#     print(text2)
#     wordDict = {}
#     for item in text1:
#         wordDict[item] = 1
#     for item in text2:
#         if item not in wordDict:
#             wordDict[item] = 1
#     array1, array2 = [], []
#     for item in wordDict:
#         if item in text1:
#             array1 += 1
#         else:
#             array1 += 0
#         if item in text2:
#             array2 += 1
#         else:
#             array2 += 0
#     return (array1,array2)
#
# def unitVector(vector):
#     return vector / np.linalg.norm(vector)
#
# def angle(v1, v2):
#     v1_u = unitVector(v1)
#     v2_u = unitVector(v2)
#     return np.arccos(np.clip(np.dot(v1_u, v2_u), -1.0, 1.0))
#
# def calculateCosine(dict1, dict2):
#     (array1,array2) = makeSimArray(dict1,dict2)
#     return angle(array1,array2)
