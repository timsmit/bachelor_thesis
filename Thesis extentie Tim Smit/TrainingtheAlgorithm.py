import pickle
import math
import time
import numpy as np
import matplotlib.pyplot as plt
from random import shuffle

from subtraction import preprocesNGramSubtractionTest
dataDict = pickle.load( open( "permutations.pkl", "rb" ) )

list = list(range(len(dataDict)))
shuffle(list)
training = list[0:int(len(dataDict)*0.5)]
validation = list[int(len(dataDict)*0.5):int(len(dataDict)*0.7)]
test = list[int(len(dataDict)*0.7):int(len(dataDict))]

def overlapNumber(x, y):
    count = 0
    for word in x:
        if word in y:
            count += 1
    return count

def overlap(dict1, dict2):
	list1 = []
	for item in dict1:
		if item not in dict2:
			list1 += [item]
	return(list1)

def makeArray(a):
	array = []
	for (item, word) in a:
		array += [word]
	return array
hoi = 0
trainingAmount = 2000
#rangeA is Length rangeB is Amount
for (rangeA, rangeB) in [(0.2,0.05)]:
    amount, ngram = 7, 4
    precision, recall = 0, 0
    sizeing = 0
    for number in test[:trainingAmount]:
        (a,b) = preprocesNGramSubtractionTest(dataDict[number][2],dataDict[number][3],4, rangeA, rangeB)
        if "No different topic was found." in [a,b] :
            hoi += 1
            print(hoi, precision)
        else:
            (a,b) = makeArray(a)[-amount:],makeArray(b)[-amount:]
            (higha,highb) = dataDict[number][0],dataDict[number][1]
            sizeing += len(dataDict[number][0])+len(dataDict[number][1])
            scorea = overlapNumber(a,higha)
            scoreb = overlapNumber(b,highb)
            precision += scorea/amount + scoreb/amount
            recall += scorea/len(higha) + scoreb/len(highb)
    av = sizeing/4000
    precision = precision/(trainingAmount-hoi)
    recall = recall/(trainingAmount-hoi) * av/amount
    print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
    print("ngram is", ngram)
    print("range A is ", rangeA)
    print("range B is ", rangeB)
    print("amount is ", amount)
    print("precision is ", precision)
    print("recall is ", recall)
    print("fscore is ", 2 * (precision*recall)/(precision+recall))
    print(amount, "&", ngram ,"&", rangeA, "&",rangeB,"&", precision ,"&", recall, "&", 2 * (precision*recall)/(precision+recall))
