import numpy as np
import re
import nltk
import matplotlib.pyplot as plt
import math
import itertools

# The algorithm proposed in the thesis of Tim Smit 2018

# Read in a data file from a .txt bestand and
def readInFile(filename):
    try:
        with open(filename) as f:
            content = f.read().lower()
            content = re.sub('[!@#$.,():?;-]*', '', content)
            return content.split()
    except:
        print("Reading in file failed")

# Make a dictionary of the words in the texts and there corresponding word count
def makeMainDict(text):
    wordDict = {}
    for item in text:
        if item not in wordDict:
            wordDict[item] = 1
        else:
            wordDict[item] += 1
    return(wordDict)

# Erasing the n-gram matched words.
def smartErase(text1, text2, i, j):
    if (len(text1) <= i):
        return (text1, text2)
    if (len(text2) <= j):
        return (text1, text2)
    while (text1[i] == text2[j]):
        text1 = text1[:(i)] + text1[i+1:]
        text2 = text2[:(j)] + text2[j+1:]
        if (len(text1) <= i):
            break
        if (len(text2) <= j):
            break
    return (text1, text2)

# Performing the n-gram subtraction
def nGramSubtraction(text1, text2, Ngram):
    count = 1
    for i in range(0,len(text1) - Ngram):
        for j in range(0,len(text2) - Ngram):
            if len(text1) > i and len(text2)>j:
                if text2[j:j + Ngram] == text1[i:i+Ngram]:
                    (text1, text2) = smartErase(text1, text2, i, j)
    return(text1,text2)

# A methode that returns the order in a list.
def returnOrder(text):
    wordDict = {}
    for item in text:
        if item not in wordDict:
            wordDict[item] = 1
        else:
            wordDict[item] += 1
    listTuple = []
    for item in reversed(sorted(wordDict, key=wordDict.__getitem__)):
        listTuple += [(item, wordDict[item])]
    return listTuple

# word selection component
def selectWord(wordlist, common, maindict):
    wordDict = {}
    for (word, i) in wordlist:
        if word in common:
            wordDict[word] = (i/(10*common[word]))
        else:
            wordDict[word] = (i/(maindict[word]-i + 1))
    inverse = [(value, key) for key, value in wordDict.items()]
    # print (inverse)
    if bool(inverse):
        return max(inverse)[1]
    return "No different topic was found."

# term completion attempt 1
def ratioTakeList(wordDict, indices, word):
    for benchmark in [0.2, 0.4, 0.6]:
        bString = word
        for item in wordDict:
            value = wordDict[item]
            if value/float(len(indices)) > benchmark:
                benchmark = value/float(len(indices))
                bString = item
        print(bString)
    return bString

# term completion attempt 1
def ratioTakeList2(wordDict, indices, word):
    string = ""
    score = 0
    for item in wordDict:
        if wordDict[item]*len(item) >= score:
            score = wordDict[item]*len(item)
            string = item
    if score > len(word)*len(indices):
        return (word, str(len(word)*len(indices)), string ,str(score))
    return (word, str(len(word)*len(indices)), string ,str(score))

# finding all the instances of a word and there surrounding n-gram
# for n-gram completion.
def wholeWordChecker(word, file):
    indices = [i for i, x in enumerate(file) if x == word]
    wordDict = {}
    takelist = []
    for i in indices:
        for j1 in range(0,2):
            for j2 in range(2,4):
                set = " ".join(file[i-j1:i+j2])
                if set in wordDict:
                    wordDict[set] +=1
                else:
                    wordDict[set] = 1
    return ratioTakeList2(wordDict, indices, word)

# A nice way to print all the steps togher
# file1 and file 2 are the names of the texts files
# common is a dict of the completely unrelated text.
def preprocesNGramSubtraction(file1, file2, common):
    # Create the ngram subtracted array from the two files
    print(file1, file2)
    word1, word2 = "", ""
    file1, file2 = readInFile(file1), readInFile(file2)
    print("This is the first text: ")
    print(file1)
    print()
    print("This is the second text: ")
    print(file2)
    print()
    maindict1, maindict2 = makeMainDict(file1), makeMainDict(file2)
    # Checking which n-gram should be taken
    (text1, text2) = nGramSubtraction(file1, file2,5)
    for i in range(1,5):
        if len(text1)/float(len(file1)) > 0.75:
            (text1, nothing) = nGramSubtraction(file1, file2,5-i)
        if len(text2)/float(len(file2)) > 0.75:
            (nothing,text2) = nGramSubtraction(file1, file2,5-i)
    print("The n-gram subtraction componenent: ")
    print("The amount words before n-gram subtraction in text1 is: ", len(file1))
    print("The amount words after n-gram subtraction in text1 is: ", len(text1))
    print("The amount words before n-gram subtraction in text2 is: ", len(file2))
    print("The amount words after n-gram subtraction in text2 is: ", len(text2))
    print()
    text1 = returnOrder(text1)
    text2 = returnOrder(text2)
    print("The top 6 returned on word count of text1 is: ", text1[0:6])
    print("The top 6 returned on word count of text2 is: ", text2[0:6])
    print()
    word1 = selectWord(text1, common, maindict1)
    word2 = selectWord(text2, common, maindict2)
    print("The selection component: ")
    print("From these words ", word1, " is selected as the word with the highest information in this context")
    print("From these words ", word2, " is selected as the word with the highest information in this context")
    print()
    wholeWord1 = wholeWordChecker(word1,file1)
    wholeWord2 = wholeWordChecker(word2,file2)
    print("The term completion component: ")
    print("In text 1:")
    print("The selected was '", wholeWord1[0], "' and this scored ", wholeWord1[1])
    print("The selected term was '", wholeWord1[2], "' and this scored ", wholeWord1[3])
    print()
    print("In text 2:")
    print("The selected was '", wholeWord2[0], "' and this scored ", wholeWord2[1])
    print("The selected term was '", wholeWord2[2], "' and this scored ", wholeWord2[3])
    return (wholeWord1, wholeWord2)

# a methode to obtain the most a completly
def mostCommon(file, common):
    feature = makeMainDict(file)
    for item in feature:
        if item in common:
            if common[item] < feature[item]:
                common[item] = feature[item]
        else:
            common[item] = feature[item]
    return common


common = {}
file0 = readInFile("similarTextDataset/00.txt")
common = mostCommon(file0, common)

i = input("Which search query would you want to know the differents about? ")
summary = preprocesNGramSubtraction("similarTextDataset/" + str(i) + "1.txt","similarTextDataset/" + str(i) + "2.txt",common)


# calculates the persentage the same
def percentageSame(file1,file2):
    infile, notinfile = 0,0
    for item in file1:
        if item in file2:
            infile += 1
        else:
            notinfile += 1
    print ("the return infile is ", infile, "the return not in file",notinfile)
    if(notinfile + infile is 0):
        return "nav"
    else:
        return infile/(notinfile+infile) * 100

# Table 2.2
def table2p2():
    for i in range(1,9):
        file1 = readInFile("similarTextDataset/" + str(i) + "1.txt")
        file2 = readInFile("similarTextDataset/" + str(i) + "2.txt")
        string1, string2 = "",""
        for i in range(1,5):
            text1,text2 = nGramSubtraction(file1,file2,i)
            string1 += " & " + str(round((len(text1)/float(len(file1)) * 100),1)) + "\%"
            string2 += " & " + str(round((len(text2)/float(len(file2)) * 100),1)) + "\%"
        print(len(file1), string1)
        print(len(file2), string2)
